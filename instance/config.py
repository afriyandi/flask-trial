__author__ = 'phe'
__created__ = '4/27/15'

DEBUG = True # Turns on debugging features in Flask
BCRYPT_LEVEL = 12 # Configuration for the Flask-Bcrypt extension
MAIL_FROM_EMAIL = "phe@work.com" # For use in application emails”
# SERVER_NAME = '0.0.0.0:5000'
SECRET_KEY = 'your secret key>'
STRIPE_API_KEY = 'your secret key'
# reserved postgres
# SQLALCHEMY_DATABASE_URI= "postgresql://user:TWljaGHFgiBCYXJ0b3N6a2lld2ljeiEh@localhost/databasename”
