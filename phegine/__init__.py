__author__ = 'phe'
__created__ = '4/27/15'

from flask import Flask

app = Flask(__name__, instance_relative_config=True)

#need custom config
#app.config.from_object('config.development')
app.config.from_object('config')
app.config.from_pyfile('config.py')

#need custom config
#app.config.from_envvar('APP_CONFIG_FILE')

@app.errorhandler(404)
def not_found(error):
    return 'Not Found', 404

from phegine.view import general
app.register_blueprint(general.mod)
