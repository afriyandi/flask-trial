__author__ = 'phe'
__created__ = '4/28/15'

from flask import Blueprint, render_template, make_response, redirect, url_for, \
     request, flash, g, jsonify, abort

mod = Blueprint('general', __name__)

@mod.route('/')
def hello_world():
    respond = make_response(render_template('index.html', title="Material Base Theme"))
    respond.set_cookie('username', 'the username')
    return respond

@mod.route('/user/')
@mod.route('/user', methods=['GET'])
def hello_user():
    go = request.args.get('name')
    if not go:
        return redirect(url_for('general.hello_world'))
    return redirect(url_for('general.hello_user_with_name', name=go))

@mod.route('/user/<name>')
def hello_user_with_name(name):
    return 'Hello {0}'.format(name)
